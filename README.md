## 💪 leetcode 记录

leetcode解题记录，解题全部使用js编写，欢迎[交流](https://github.com/theajack/leetcode/issues/new)

## 解题列表

1. [二进制数1的最大间距](binary-gap.md)
2. [链表倒数第k个元素-双指针](list-last-k.md)
3. [二叉树的后序遍历](binary-tree-postorder-traversal.md)
4. [三步走楼梯问题-动态规划](step.md)
5. [奇偶链表问题-逢奇调换算法和奇偶队列拼接算法](odd-even-link.md)
6. [顺时针打印矩阵](log-matrix.md)

## 友情链接

[cnchar](https://www.github.com/theajack/cnchar) | [easy-icon](https://www.github.com/theajack/easy-icon) | [jsbox](https://www.github.com/theajack/jsbox)

该库所有题目来自 [leetcode](https://leetcode-cn.com/)
